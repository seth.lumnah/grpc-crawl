# gPRC Crawler

## Build

1. Make sure you have `protoc` and `protoc-gen-go` in `$PATH`.
2. Run `./build.sh`. This will install `crawld` (local service) and `crawl` (command line client) into `./bin`.

# Run

1. Start the service: `./bin/crawld`.
2. To start crawling a site: `./bin/crawl -start example.com`
3. To stop crawling a site: `./bin/crawl -stop example.com`
4. To print the site tree(s): `./bin/crawl -list`

Notes:

- Both the `start` and `stop` command take a list of sites. If more than one is provided, then each will be crawled concurrently. 
- The service listens on `localhost:9001` by default. Use the `-addr` flag with both the service and the client use a different port.
- The crawler only looks for `<a>` elements with the `href` attribute served in the HTML (JavaScript is not processed). Fragment URLs are ignore. URLs are followed if the URL specifies the same host (domain and port), or if the URL does not include a host.
- Recursive loops are not detected and no optimizations have been made for when traversing a URL more than once.