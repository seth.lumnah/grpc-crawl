package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strings"

	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"

	"gitlab.com/seth.lumnah/grpc-crawl/internal/crawler"
	"gitlab.com/seth.lumnah/grpc-crawl/internal/ctxutil"
)

func start(ctx context.Context, c crawler.CrawlerClient, url ...string) error {
	if _, err := c.Start(ctx, &crawler.StartRequest{URLs: url}); err != nil {
		return fmt.Errorf("client: start [%s]: %w", url, err)
	}

	return nil
}

func stop(ctx context.Context, c crawler.CrawlerClient, url ...string) error {
	if _, err := c.Stop(ctx, &crawler.StopRequest{URLs: url}); err != nil {
		return fmt.Errorf("client: stop [%s]: %w", url, err)
	}

	return nil
}

func printTree(w io.Writer, t *crawler.Tree, depth int) error {
	if _, err := fmt.Fprintf(w, "%s%s\n", strings.Repeat("\t", depth), t.GetURL()); err != nil {
		return err
	}

	for _, child := range t.GetChildren() {
		if err := printTree(w, child, depth+1); err != nil {
			return err
		}
	}

	return nil
}

func list(ctx context.Context, w io.Writer, c crawler.CrawlerClient) error {
	client, err := c.List(ctx, &crawler.ListRequest{})
	if err != nil {
		return fmt.Errorf("client: list: %w", err)
	}

	g, ctx := errgroup.WithContext(ctx)

	g.Go(func() error {
		for {
			select {
			case <-ctx.Done():
				return nil
			default:
				resp, err := client.Recv()

				switch {
				case errors.Is(err, io.EOF):
					return nil
				case err != nil:
					return err
				}

				if err := printTree(w, resp.GetTree(), 0); err != nil {
					return err
				}
			}
		}
	})

	return g.Wait()
}

func run(ctx context.Context, args []string, stdin io.Reader, stdout, stderr io.Writer) error {
	var (
		cmdStart, cmdStop, cmdList bool
		addr                       string
	)

	fs := flag.NewFlagSet(args[0], flag.ContinueOnError)
	fs.SetOutput(stderr)
	fs.BoolVar(&cmdStart, "start", false, "signals the service to start crawling www.example.com")
	fs.BoolVar(&cmdStop, "stop", false, "signals the service to stop crawling www.example.com")
	fs.BoolVar(&cmdList, "list", false, "shows the current \"site tree\" for all crawled URLs")
	fs.StringVar(&addr, "addr", "localhost:9001", "network address of the server")

	if err := fs.Parse(args[1:]); err != nil {
		fs.PrintDefaults()
		return nil
	}

	switch {
	case cmdStart && !cmdStop && !cmdList:
	case cmdStop && !cmdStart && !cmdList:
	case cmdList && !cmdStart && !cmdStop:
	case !cmdStart && !cmdStop && !cmdList:
		fs.PrintDefaults()
		return nil
	default:
		return fmt.Errorf("only one operation (start|stop|list) at a time")
	}

	conn, err := grpc.Dial(addr, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		return err
	}
	defer conn.Close()

	c := crawler.NewCrawlerClient(conn)

	switch {
	case cmdStart:
		return start(ctx, c, fs.Args()...)
	case cmdStop:
		return stop(ctx, c, fs.Args()...)
	case cmdList:
		return list(ctx, stdout, c)
	default:
		return nil
	}
}

func main() {
	ctx, cancel := ctxutil.WithSignal(context.Background(), os.Interrupt)
	defer cancel()

	if err := run(ctx, os.Args, os.Stdin, os.Stdout, os.Stderr); err != nil {
		log.Fatal(err)
	}
}
