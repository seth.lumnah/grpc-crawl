//go:generate protoc --proto_path ../../internal/crawler --go_out=plugins=grpc:../../internal/crawler ../../internal/crawler/crawler.proto

package main

import (
	"context"
	"flag"
	"io"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"sync"

	"golang.org/x/net/html"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"

	"gitlab.com/seth.lumnah/grpc-crawl/internal/crawler"
	"gitlab.com/seth.lumnah/grpc-crawl/internal/ctxutil"
)

type Tree struct {
	*crawler.Tree
	cancel context.CancelFunc
}

type server struct {
	crawler.UnimplementedCrawlerServer

	logger      *log.Logger
	start, stop chan string

	mu    sync.RWMutex    // protects trees
	trees map[string]Tree // requested url to tree
}

func newServer(logger *log.Logger) *server {
	return &server{
		logger: logger,
		start:  make(chan string),
		stop:   make(chan string),
		trees:  map[string]Tree{},
	}
}

func follow(a, b string) string {
	x, err := url.Parse(a)
	if err != nil {
		return ""
	}

	y, err := x.Parse(b)
	if err != nil {
		return ""
	}

	y.RawQuery = ""
	y.Fragment = ""

	switch {
	case x.Host != y.Host:
		fallthrough
	case x.Path == y.Path:
		return ""
	default:
		return y.String()
	}
}

// crawl crawls url to find and recursively crawl all <a href> links to the same site.
// ! Note: crawl does not identify recursive links nor already followed links.
func crawl(ctx context.Context, t *crawler.Tree, depth int, logger *log.Logger) {
	c := http.DefaultClient

	u, err := url.Parse(t.URL)
	if err != nil {
		logger.Println(err)
		return
	}

	if u.Scheme == "" {
		u.Scheme = "https"
	}

	t.URL = u.String()
	logger.Printf("crawling %s\n", t.URL)

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, t.URL, nil)
	if err != nil {
		logger.Println(err)
		return
	}

	resp, err := c.Do(req)
	if err != nil {
		logger.Println(err)
		return
	}
	defer resp.Body.Close()

	doc, err := html.Parse(resp.Body)
	if err != nil {
		logger.Println(err)
		return
	}

	var f func(*html.Node)
	f = func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "a" {
			for _, a := range n.Attr {
				if a.Key == "href" {
					u := follow(t.URL, a.Val)
					if u == "" {
						break
					}

					child := &crawler.Tree{URL: u}
					t.Children = append(t.Children, child)

					crawl(ctx, child, depth+1, logger)
					break
				}
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}

	done := make(chan struct{})
	go func() {
		f(doc)
		done <- struct{}{}
	}()

	select {
	case <-done:
		return
	case <-ctx.Done():
		return
	}
}

func (s *server) startCrawl(ctx context.Context) {
	for {
		select {
		case url := <-s.start:
			s.mu.Lock()

			if t := s.trees[url]; t.cancel == nil {
				ctx2, cancel := context.WithCancel(ctx)
				t.Tree = &crawler.Tree{URL: url}
				t.cancel = cancel
				s.trees[url] = t

				go crawl(ctx2, t.Tree, 0, s.logger)
			}

			s.mu.Unlock()

		case url := <-s.stop:
			s.mu.Lock()

			if t, ok := s.trees[url]; ok {
				t.cancel()
				t.cancel = nil
			}

			s.mu.Unlock()

		case <-ctx.Done():
			return
		}
	}
}

func (s *server) Start(ctx context.Context, req *crawler.StartRequest) (*crawler.StartResponse, error) {
	resp := &crawler.StartResponse{}

	if req == nil {
		return resp, nil
	}

	s.logger.Printf("start crawling: %s\n", req.GetURLs())

	for _, url := range req.GetURLs() {
		s.start <- url
	}

	return resp, nil
}

func (s *server) Stop(ctx context.Context, req *crawler.StopRequest) (*crawler.StopResponse, error) {
	resp := &crawler.StopResponse{}

	if req == nil {
		return resp, nil
	}

	s.logger.Printf("stop crawling: %s\n", req.GetURLs())

	for _, url := range req.GetURLs() {
		s.stop <- url
	}

	return resp, nil
}

func (s *server) List(req *crawler.ListRequest, srv crawler.Crawler_ListServer) error {
	s.mu.RLock()
	defer s.mu.RUnlock()

	for _, t := range s.trees {
		if err := srv.Send(&crawler.ListResponse{Tree: t.Tree}); err != nil {
			return err
		}
	}

	return nil
}

func run(ctx context.Context, args []string, stdin io.Reader, stdout, stderr io.Writer) error {
	var (
		addr string
	)

	fs := flag.NewFlagSet(args[0], flag.ContinueOnError)
	fs.SetOutput(stderr)
	fs.StringVar(&addr, "addr", "localhost:9001", "network address of the server")

	if err := fs.Parse(args[1:]); err != nil {
		fs.PrintDefaults()
		return nil
	}

	lis, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}

	svr := newServer(log.New(stderr, "", log.LstdFlags))
	go svr.startCrawl(ctx)

	s := grpc.NewServer()
	crawler.RegisterCrawlerServer(s, svr)

	g := errgroup.Group{}

	g.Go(func() error {
		return s.Serve(lis)
	})

	g.Go(func() error {
		<-ctx.Done()
		s.GracefulStop()
		return nil
	})

	return g.Wait()
}

func main() {
	ctx, cancel := ctxutil.WithSignal(context.Background(), os.Interrupt)
	defer cancel()

	if err := run(ctx, os.Args, os.Stdin, os.Stdout, os.Stderr); err != nil {
		log.Fatal(err)
	}
}
