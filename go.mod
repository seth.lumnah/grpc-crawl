module gitlab.com/seth.lumnah/grpc-crawl

go 1.14

require (
	github.com/golang/protobuf v1.3.4
	golang.org/x/net v0.0.0-20200301022130-244492dfa37a
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e
	google.golang.org/grpc v1.28.0
)
