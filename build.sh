cd "$(dirname $(go env GOMOD))"

export GOBIN="${PWD}/bin"
export PATH=${PATH}:"${GOBIN}"

mkdir -p "${GOBIN}"

go generate gitlab.com/seth.lumnah/grpc-crawl/...
go install ./cmd/crawl
go install ./cmd/crawld
