package ctxutil

import (
	"context"
	"os"
	"os/signal"
)

func WithSignal(parent context.Context, sig ...os.Signal) (context.Context, context.CancelFunc) {
	ctx, cancel := context.WithCancel(parent)

	ch := make(chan os.Signal, 1)
	signal.Notify(ch, sig...)

	go func() {
		select {
		case <-ctx.Done():
		case <-ch:
			cancel()
		}

		signal.Stop(ch)
		close(ch)
	}()

	return ctx, cancel
}
