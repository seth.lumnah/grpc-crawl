// Code generated by protoc-gen-go. DO NOT EDIT.
// source: crawler.proto

package crawler

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type Tree struct {
	URL                  string   `protobuf:"bytes,1,opt,name=URL,proto3" json:"URL,omitempty"`
	Children             []*Tree  `protobuf:"bytes,2,rep,name=Children,proto3" json:"Children,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Tree) Reset()         { *m = Tree{} }
func (m *Tree) String() string { return proto.CompactTextString(m) }
func (*Tree) ProtoMessage()    {}
func (*Tree) Descriptor() ([]byte, []int) {
	return fileDescriptor_84c7eabcfe7807d1, []int{0}
}

func (m *Tree) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Tree.Unmarshal(m, b)
}
func (m *Tree) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Tree.Marshal(b, m, deterministic)
}
func (m *Tree) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Tree.Merge(m, src)
}
func (m *Tree) XXX_Size() int {
	return xxx_messageInfo_Tree.Size(m)
}
func (m *Tree) XXX_DiscardUnknown() {
	xxx_messageInfo_Tree.DiscardUnknown(m)
}

var xxx_messageInfo_Tree proto.InternalMessageInfo

func (m *Tree) GetURL() string {
	if m != nil {
		return m.URL
	}
	return ""
}

func (m *Tree) GetChildren() []*Tree {
	if m != nil {
		return m.Children
	}
	return nil
}

type StartRequest struct {
	URLs                 []string `protobuf:"bytes,1,rep,name=URLs,proto3" json:"URLs,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *StartRequest) Reset()         { *m = StartRequest{} }
func (m *StartRequest) String() string { return proto.CompactTextString(m) }
func (*StartRequest) ProtoMessage()    {}
func (*StartRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_84c7eabcfe7807d1, []int{1}
}

func (m *StartRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_StartRequest.Unmarshal(m, b)
}
func (m *StartRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_StartRequest.Marshal(b, m, deterministic)
}
func (m *StartRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_StartRequest.Merge(m, src)
}
func (m *StartRequest) XXX_Size() int {
	return xxx_messageInfo_StartRequest.Size(m)
}
func (m *StartRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_StartRequest.DiscardUnknown(m)
}

var xxx_messageInfo_StartRequest proto.InternalMessageInfo

func (m *StartRequest) GetURLs() []string {
	if m != nil {
		return m.URLs
	}
	return nil
}

type StartResponse struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *StartResponse) Reset()         { *m = StartResponse{} }
func (m *StartResponse) String() string { return proto.CompactTextString(m) }
func (*StartResponse) ProtoMessage()    {}
func (*StartResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_84c7eabcfe7807d1, []int{2}
}

func (m *StartResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_StartResponse.Unmarshal(m, b)
}
func (m *StartResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_StartResponse.Marshal(b, m, deterministic)
}
func (m *StartResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_StartResponse.Merge(m, src)
}
func (m *StartResponse) XXX_Size() int {
	return xxx_messageInfo_StartResponse.Size(m)
}
func (m *StartResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_StartResponse.DiscardUnknown(m)
}

var xxx_messageInfo_StartResponse proto.InternalMessageInfo

type StopRequest struct {
	URLs                 []string `protobuf:"bytes,1,rep,name=URLs,proto3" json:"URLs,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *StopRequest) Reset()         { *m = StopRequest{} }
func (m *StopRequest) String() string { return proto.CompactTextString(m) }
func (*StopRequest) ProtoMessage()    {}
func (*StopRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_84c7eabcfe7807d1, []int{3}
}

func (m *StopRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_StopRequest.Unmarshal(m, b)
}
func (m *StopRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_StopRequest.Marshal(b, m, deterministic)
}
func (m *StopRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_StopRequest.Merge(m, src)
}
func (m *StopRequest) XXX_Size() int {
	return xxx_messageInfo_StopRequest.Size(m)
}
func (m *StopRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_StopRequest.DiscardUnknown(m)
}

var xxx_messageInfo_StopRequest proto.InternalMessageInfo

func (m *StopRequest) GetURLs() []string {
	if m != nil {
		return m.URLs
	}
	return nil
}

type StopResponse struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *StopResponse) Reset()         { *m = StopResponse{} }
func (m *StopResponse) String() string { return proto.CompactTextString(m) }
func (*StopResponse) ProtoMessage()    {}
func (*StopResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_84c7eabcfe7807d1, []int{4}
}

func (m *StopResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_StopResponse.Unmarshal(m, b)
}
func (m *StopResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_StopResponse.Marshal(b, m, deterministic)
}
func (m *StopResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_StopResponse.Merge(m, src)
}
func (m *StopResponse) XXX_Size() int {
	return xxx_messageInfo_StopResponse.Size(m)
}
func (m *StopResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_StopResponse.DiscardUnknown(m)
}

var xxx_messageInfo_StopResponse proto.InternalMessageInfo

type ListRequest struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ListRequest) Reset()         { *m = ListRequest{} }
func (m *ListRequest) String() string { return proto.CompactTextString(m) }
func (*ListRequest) ProtoMessage()    {}
func (*ListRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_84c7eabcfe7807d1, []int{5}
}

func (m *ListRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ListRequest.Unmarshal(m, b)
}
func (m *ListRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ListRequest.Marshal(b, m, deterministic)
}
func (m *ListRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ListRequest.Merge(m, src)
}
func (m *ListRequest) XXX_Size() int {
	return xxx_messageInfo_ListRequest.Size(m)
}
func (m *ListRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_ListRequest.DiscardUnknown(m)
}

var xxx_messageInfo_ListRequest proto.InternalMessageInfo

type ListResponse struct {
	Tree                 *Tree    `protobuf:"bytes,1,opt,name=Tree,proto3" json:"Tree,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ListResponse) Reset()         { *m = ListResponse{} }
func (m *ListResponse) String() string { return proto.CompactTextString(m) }
func (*ListResponse) ProtoMessage()    {}
func (*ListResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_84c7eabcfe7807d1, []int{6}
}

func (m *ListResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ListResponse.Unmarshal(m, b)
}
func (m *ListResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ListResponse.Marshal(b, m, deterministic)
}
func (m *ListResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ListResponse.Merge(m, src)
}
func (m *ListResponse) XXX_Size() int {
	return xxx_messageInfo_ListResponse.Size(m)
}
func (m *ListResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_ListResponse.DiscardUnknown(m)
}

var xxx_messageInfo_ListResponse proto.InternalMessageInfo

func (m *ListResponse) GetTree() *Tree {
	if m != nil {
		return m.Tree
	}
	return nil
}

func init() {
	proto.RegisterType((*Tree)(nil), "crawler.Tree")
	proto.RegisterType((*StartRequest)(nil), "crawler.StartRequest")
	proto.RegisterType((*StartResponse)(nil), "crawler.StartResponse")
	proto.RegisterType((*StopRequest)(nil), "crawler.StopRequest")
	proto.RegisterType((*StopResponse)(nil), "crawler.StopResponse")
	proto.RegisterType((*ListRequest)(nil), "crawler.ListRequest")
	proto.RegisterType((*ListResponse)(nil), "crawler.ListResponse")
}

func init() {
	proto.RegisterFile("crawler.proto", fileDescriptor_84c7eabcfe7807d1)
}

var fileDescriptor_84c7eabcfe7807d1 = []byte{
	// 257 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x7c, 0x91, 0x3f, 0x4f, 0xc3, 0x30,
	0x10, 0xc5, 0x6b, 0x12, 0x28, 0xb9, 0xd4, 0x80, 0x4e, 0x14, 0x45, 0x9d, 0x52, 0x4f, 0x61, 0xa9,
	0xa0, 0x08, 0xc1, 0x4c, 0xd6, 0x4c, 0x2e, 0x5d, 0xd8, 0x0a, 0x58, 0xa2, 0x52, 0x55, 0x07, 0xdb,
	0x88, 0x0f, 0xc6, 0x17, 0x44, 0xfe, 0x13, 0xcb, 0x2a, 0x12, 0x9b, 0xef, 0xee, 0xbd, 0xf3, 0xfb,
	0xe9, 0x80, 0xbe, 0xa9, 0xcd, 0xf7, 0x4e, 0xa8, 0x45, 0xaf, 0xa4, 0x91, 0x38, 0x0e, 0x25, 0x6b,
	0x21, 0x7f, 0x56, 0x42, 0xe0, 0x05, 0x64, 0x6b, 0xde, 0x55, 0xa4, 0x26, 0x4d, 0xc1, 0xed, 0x13,
	0xaf, 0xe1, 0xb4, 0xfd, 0xd8, 0xee, 0xde, 0x95, 0xd8, 0x57, 0x47, 0x75, 0xd6, 0x94, 0x4b, 0xba,
	0x18, 0x96, 0x58, 0x0b, 0x8f, 0x63, 0xc6, 0x60, 0xb2, 0x32, 0x1b, 0x65, 0xb8, 0xf8, 0xfc, 0x12,
	0xda, 0x20, 0x42, 0xbe, 0xe6, 0x9d, 0xae, 0x48, 0x9d, 0x35, 0x05, 0x77, 0x6f, 0x76, 0x0e, 0x34,
	0x68, 0x74, 0x2f, 0xf7, 0x5a, 0xb0, 0x39, 0x94, 0x2b, 0x23, 0xfb, 0xff, 0x3c, 0x67, 0x76, 0xaf,
	0x95, 0x04, 0x0b, 0x85, 0xb2, 0xdb, 0xea, 0xe1, 0x1b, 0x76, 0x0b, 0x13, 0x5f, 0xfa, 0x31, 0xce,
	0x3d, 0x8b, 0x83, 0xf8, 0x93, 0xd6, 0x8d, 0x96, 0x3f, 0x04, 0xc6, 0xad, 0x6f, 0xe3, 0x23, 0x1c,
	0xbb, 0x44, 0x38, 0x8d, 0xca, 0x94, 0x62, 0x76, 0x75, 0xd8, 0x0e, 0x29, 0x46, 0x78, 0x0f, 0xb9,
	0xcd, 0x85, 0x97, 0x89, 0x22, 0x92, 0xcc, 0xa6, 0x07, 0xdd, 0x68, 0x7b, 0x80, 0xdc, 0xe6, 0x4d,
	0x6c, 0x09, 0x4d, 0x62, 0x4b, 0xa1, 0xd8, 0xe8, 0x86, 0x3c, 0x15, 0x2f, 0xc3, 0xbd, 0x5e, 0x4f,
	0xdc, 0xfd, 0xee, 0x7e, 0x03, 0x00, 0x00, 0xff, 0xff, 0x69, 0x7f, 0x53, 0xcd, 0xd0, 0x01, 0x00,
	0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConnInterface

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion6

// CrawlerClient is the client API for Crawler service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type CrawlerClient interface {
	Start(ctx context.Context, in *StartRequest, opts ...grpc.CallOption) (*StartResponse, error)
	Stop(ctx context.Context, in *StopRequest, opts ...grpc.CallOption) (*StopResponse, error)
	List(ctx context.Context, in *ListRequest, opts ...grpc.CallOption) (Crawler_ListClient, error)
}

type crawlerClient struct {
	cc grpc.ClientConnInterface
}

func NewCrawlerClient(cc grpc.ClientConnInterface) CrawlerClient {
	return &crawlerClient{cc}
}

func (c *crawlerClient) Start(ctx context.Context, in *StartRequest, opts ...grpc.CallOption) (*StartResponse, error) {
	out := new(StartResponse)
	err := c.cc.Invoke(ctx, "/crawler.Crawler/Start", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *crawlerClient) Stop(ctx context.Context, in *StopRequest, opts ...grpc.CallOption) (*StopResponse, error) {
	out := new(StopResponse)
	err := c.cc.Invoke(ctx, "/crawler.Crawler/Stop", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *crawlerClient) List(ctx context.Context, in *ListRequest, opts ...grpc.CallOption) (Crawler_ListClient, error) {
	stream, err := c.cc.NewStream(ctx, &_Crawler_serviceDesc.Streams[0], "/crawler.Crawler/List", opts...)
	if err != nil {
		return nil, err
	}
	x := &crawlerListClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

type Crawler_ListClient interface {
	Recv() (*ListResponse, error)
	grpc.ClientStream
}

type crawlerListClient struct {
	grpc.ClientStream
}

func (x *crawlerListClient) Recv() (*ListResponse, error) {
	m := new(ListResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// CrawlerServer is the server API for Crawler service.
type CrawlerServer interface {
	Start(context.Context, *StartRequest) (*StartResponse, error)
	Stop(context.Context, *StopRequest) (*StopResponse, error)
	List(*ListRequest, Crawler_ListServer) error
}

// UnimplementedCrawlerServer can be embedded to have forward compatible implementations.
type UnimplementedCrawlerServer struct {
}

func (*UnimplementedCrawlerServer) Start(ctx context.Context, req *StartRequest) (*StartResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Start not implemented")
}
func (*UnimplementedCrawlerServer) Stop(ctx context.Context, req *StopRequest) (*StopResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Stop not implemented")
}
func (*UnimplementedCrawlerServer) List(req *ListRequest, srv Crawler_ListServer) error {
	return status.Errorf(codes.Unimplemented, "method List not implemented")
}

func RegisterCrawlerServer(s *grpc.Server, srv CrawlerServer) {
	s.RegisterService(&_Crawler_serviceDesc, srv)
}

func _Crawler_Start_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(StartRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CrawlerServer).Start(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/crawler.Crawler/Start",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CrawlerServer).Start(ctx, req.(*StartRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Crawler_Stop_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(StopRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CrawlerServer).Stop(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/crawler.Crawler/Stop",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CrawlerServer).Stop(ctx, req.(*StopRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Crawler_List_Handler(srv interface{}, stream grpc.ServerStream) error {
	m := new(ListRequest)
	if err := stream.RecvMsg(m); err != nil {
		return err
	}
	return srv.(CrawlerServer).List(m, &crawlerListServer{stream})
}

type Crawler_ListServer interface {
	Send(*ListResponse) error
	grpc.ServerStream
}

type crawlerListServer struct {
	grpc.ServerStream
}

func (x *crawlerListServer) Send(m *ListResponse) error {
	return x.ServerStream.SendMsg(m)
}

var _Crawler_serviceDesc = grpc.ServiceDesc{
	ServiceName: "crawler.Crawler",
	HandlerType: (*CrawlerServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Start",
			Handler:    _Crawler_Start_Handler,
		},
		{
			MethodName: "Stop",
			Handler:    _Crawler_Stop_Handler,
		},
	},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "List",
			Handler:       _Crawler_List_Handler,
			ServerStreams: true,
		},
	},
	Metadata: "crawler.proto",
}
